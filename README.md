# Dynamic DNS using Rackspace Cloud

Taken from: https://matthew.komputerwiz.net/2014/04/15/dynamic-dns-with-rackspace.html and updated a little

> Props to https://twitter.com/komputerwiz for the original version

# Install

## Setup

You will need Python on the path and these libraries:
* `python-dateutil`
* `requests`
* `pytz`

Depending on where it breaks you may need to install python, and or install a library

    $ pip install requests python-dateutil pytz


## Copy the files

    $ sudo mkdir -p /opt/ddnsrs
    $ sudo chown ${USER} /opt/ddnsrs
    $ cd /opt/ddnsrs
    $ git clone https://gitlab.com/95point2/ddnsrs.git .
 
## Configure it

    $ cp /opt/ddnsrs/config.json.tpl /opt/ddnsrs/config.json
    $ cp /opt/ddnsrs/cache.json.tpl /opt/ddnsrs/cache.json

Then edit config.json with your rackspace cloud DNS account, domain and record settings

## Rinse

    $ /opt/ddnsrs/update >> /opt/ddnsrs/log/update-$(date +\%Y\%m).log 2>&1

You should see an entry in the log/ folder 

## Repeat

    $ crontab -e

and enter a line like the following.....

    */20 * * * * /opt/ddnsrs/update >> /opt/ddnsrs/log/update-$(date +%Y%m).log 2>&1


The example above runs the updater every 20 minutes.  Tune it depending what you need.

